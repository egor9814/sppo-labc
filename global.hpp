//
// Created by egor9814 on 29.03.19.
//

#ifndef SPPLABC_GLOBAL_HPP
#define SPPLABC_GLOBAL_HPP

#include "cpp_model.hpp"
#include "java_model.hpp"
#include "cs_model.hpp"

using DefaultLanguageModel = spp::JavaModel;

using DefaultLanguage = spp::LangFabric<DefaultLanguageModel>;

#endif //SPPLABC_GLOBAL_HPP
