//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_UNIT_HPP
#define SPPLABC_UNIT_HPP

#include <memory>
#include <exception>
#include <string>
#include <sstream>

namespace spp {

    class Unit;
    using UnitPtr = std::shared_ptr<Unit>;

    class Unit {
    public:
        using Flag = unsigned int;

        virtual ~Unit() = default;

        virtual void add(const UnitPtr&, Flag flags = 0) {
            throw std::runtime_error("Not supported");
        }

        virtual std::string compile(unsigned int level = 0) const = 0;

    protected:
        virtual std::string generateShift(unsigned int level) const {
            return std::string(level * 4, ' ');
        }
    };

}

#endif //SPPLABC_UNIT_HPP
