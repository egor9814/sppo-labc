//
// Created by egor9814 on 26.03.19.
//

#ifndef SPPLABC_MODEL_HPP
#define SPPLABC_MODEL_HPP

#include "unit.hpp"
#include "class_unit.hpp"
#include "method_unit.hpp"
#include "print_op_unit.hpp"

namespace spp {

    template <typename LanguageTag> struct LangModel;

    template <typename LanguageTag>
    class LangFabric {
        LangFabric() = default;

    public:
        using Model = LangModel<LanguageTag>;
        using Class = typename Model::ClassUnitType;
        using Method = typename Model::MethodUnitType;
        using PrintOperator = typename Model::PrintOperatorUnitType;
        using ReturnOperator = typename Model::ReturnOperatorUnitType;

        static UnitPtr newClass(const std::string& name) {
            return std::make_shared<Class>(name);
        }

        static UnitPtr newMethod(const std::string& name, const std::string& returnType, Unit::Flag flags) {
            return std::make_shared<Method>(name, returnType, flags);
        }

        static UnitPtr newPrint(const std::string& text) {
            return std::make_shared<PrintOperator>(text);
        }

        static UnitPtr newReturn(const std::string& returnValue = "") {
            return std::make_shared<ReturnOperator>(returnValue);
        }
    };


    class InvalidModelException : public std::runtime_error {
    public:
        explicit InvalidModelException(const std::string &__arg) : runtime_error(__arg) {}

        explicit InvalidModelException(const char *__arg) : runtime_error(__arg){}

        InvalidModelException(const InvalidModelException &__arg) = default;
    };

}

#endif //SPPLABC_MODEL_HPP
