#include <utility>

//
// Created by egor9814 on 29.03.19.
//

#ifndef SPPLABC_RETURN_OP_UNIT_HPP
#define SPPLABC_RETURN_OP_UNIT_HPP

namespace spp {

    class ReturnOperatorUnit : public Unit {
    protected:
        std::string value;

    public:
        explicit ReturnOperatorUnit(std::string returnValue) : value(std::move(returnValue)) {}
    };

}

#endif //SPPLABC_RETURN_OP_UNIT_HPP
