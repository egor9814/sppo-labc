#include <iostream>
#include "global.hpp"
#include "model.hpp"

std::string generateDefaultProgram() {
    using Fabric = DefaultLanguage;

    auto cls = Fabric::newClass("TestCase");

    auto test1 = Fabric::newMethod("test1", "int", Fabric::Method::CONST);
    test1->add(
            Fabric::newPrint("this is test #1\\n")
            );
    test1->add(
            Fabric::newReturn("1")
            );
    cls->add(test1);

    cls->add(
            Fabric::newMethod("main", "void", Fabric::Method::STATIC),
            Fabric::Class::PUBLIC
            );

    return cls->compile();
}

template <typename Language>
std::string generateProgram() {
    using Fabric = spp::LangFabric<Language>;

    auto myClass = Fabric::newClass("MyClass");
    myClass->add(
            Fabric::newMethod("testFunc1", "void", 0),
            Fabric::Class::PUBLIC
            );
    myClass->add(
            Fabric::newMethod("testFunc2", "void", Fabric::Method::STATIC),
            Fabric::Class::PRIVATE
            );
    myClass->add(
            Fabric::newMethod("testFunc3", "void",
                    Fabric::Method::VIRTUAL | Fabric::Method::CONST),
            Fabric::Class::PUBLIC
            );
    auto method = Fabric::newMethod("testFunc4", "void", Fabric::Method::STATIC);
    method->add(Fabric::newPrint(R"(Hello, World!\n)"));
    myClass->add(method, Fabric::Class::PROTECTED);
    return myClass->compile();
}


int main() {
    using namespace spp;

    try {
        std::cout << "C++ Program:" << std::endl;
        std::cout << generateProgram<CPPModel>() << std::endl << std::endl;

        std::cout << "Java Program:" << std::endl;
        std::cout << generateProgram<JavaModel>() << std::endl << std::endl;

        std::cout << "C# Program:" << std::endl;
        std::cout << generateProgram<CSModel>() << std::endl << std::endl;

        std::cout << "Default language:" << std::endl;
        std::cout << generateDefaultProgram() << std::endl << std::endl;
    } catch (const InvalidModelException& err) {
        std::cerr << err.what() << std::endl;
    }
    return 0;
}