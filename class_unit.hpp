#include <utility>

//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_CLASS_UNIT_HPP
#define SPPLABC_CLASS_UNIT_HPP

#include "unit.hpp"
#include <vector>

namespace spp {

    class ClassUnit : public Unit {
    protected:
        std::string className;
        using Fields = std::vector<UnitPtr>;
        std::vector<Fields> fields;

    public:
        enum AccessModifier : Unit::Flag {
            PRIVATE = 0, PUBLIC, PROTECTED
        };

        static const std::vector<std::string> ACCESS_MODIFIERS;

        explicit ClassUnit(std::string name) : className(std::move(name)) {
            fields.resize(ACCESS_MODIFIERS.size());
        }

        void add(const UnitPtr &ptr, Flag flags) override {
            if (!ptr)
                return;

            Flag access = PRIVATE;
            if (flags < ACCESS_MODIFIERS.size()) {
                access = flags;
            }
            fields[access].push_back(ptr);
        }

    };
    const std::vector<std::string> ClassUnit::ACCESS_MODIFIERS = { "private", "public", "protected" };

}

#endif //SPPLABC_CLASS_UNIT_HPP
