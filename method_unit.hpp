//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_METHOD_UNIT_HPP
#define SPPLABC_METHOD_UNIT_HPP

#include "unit.hpp"
#include <vector>

namespace spp {

    class MethodUnit : public Unit {
    protected:
        std::string methodName, returnType;
        Flag flags;
        std::vector<UnitPtr> body;

    public:
        enum Modifier {
            STATIC = 1,
            CONST = 1 << 1,
            VIRTUAL = 1 << 2
        };

        MethodUnit(std::string name, std::string returnType, Flag flags)
            : methodName(std::move(name)), returnType(std::move(returnType)), flags(flags) {}

        void add(const UnitPtr &ptr, /*unused*/ Flag /*flags*/ = 0) override {
            if (ptr)
                body.push_back(ptr);
        }

    };

}

#endif //SPPLABC_METHOD_UNIT_HPP
