//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_JAVA_MODEL_HPP
#define SPPLABC_JAVA_MODEL_HPP

#include "unit.hpp"
#include "class_unit.hpp"
#include "method_unit.hpp"
#include "print_op_unit.hpp"

namespace spp {

    class JavaClassUnit final : public ClassUnit {
    public:
        explicit JavaClassUnit(std::string name) : ClassUnit(std::move(name)) {}

        std::string compile(unsigned int level = 0) const override {
            std::stringstream result;
            result << generateShift(level) << "class " << className << " {" << std::endl;

            for (size_t i = 0; i < ACCESS_MODIFIERS.size(); i++) {
                if (!fields[i].empty()) {
                    for (auto &it : fields[i]) {
                        result << std::endl << generateShift(level+1)
                               << ACCESS_MODIFIERS[i] << " " << it->compile(level+1);
                    }
                }
            }
            result << generateShift(level) << "}" << std::endl;
            return result.str();
        }

    };


    class JavaMethodUnit final : public MethodUnit {
    public:
        JavaMethodUnit(std::string name, std::string returnType, Flag flags)
                : MethodUnit(std::move(name), std::move(returnType), flags) {}

        std::string compile(unsigned int level) const override {
            std::stringstream result;
            if (flags & STATIC) {
                result << "static ";
            } else if (!(flags & VIRTUAL)) {
                result << "final ";
            }

            result << returnType << " " << methodName << "() {" << std::endl;

            for (auto &it : body) {
                result << it->compile(level + 1);
            }

            result << generateShift(level) << '}' << std::endl;

            return result.str();
        }

    };


    class JavaPrintOperatorUnit final : public PrintOperatorUnit {
    public:
        explicit JavaPrintOperatorUnit(std::string text) : PrintOperatorUnit(std::move(text)) {}

        std::string compile(unsigned int level) const override {
            return generateShift(level) + "System.out.print(\"" + text + "\");\n";
        }

    };


    class JavaReturnOperatorUnit final : public ReturnOperatorUnit {
    public:
        explicit JavaReturnOperatorUnit(const std::string &returnValue = "")
                : ReturnOperatorUnit(returnValue) {}

        std::string compile(unsigned int level) const override {
            auto result = generateShift(level) + "return";
            if (!value.empty())
                result += " " + value;
            return result + ";\n";
        }

    };


    struct JavaModel;
    template <> struct LangModel<JavaModel> {
        using ClassUnitType = JavaClassUnit;
        using MethodUnitType = JavaMethodUnit;
        using PrintOperatorUnitType = JavaPrintOperatorUnit;
        using ReturnOperatorUnitType = JavaReturnOperatorUnit;
    };

    using JavaLangFabric = LangFabric<JavaModel>;
}

#endif //SPPLABC_JAVA_MODEL_HPP
