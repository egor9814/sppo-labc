//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_CS_MODEL_HPP
#define SPPLABC_CS_MODEL_HPP

#include "unit.hpp"
#include "class_unit.hpp"
#include "method_unit.hpp"
#include "print_op_unit.hpp"

namespace spp {

    class CSClassUnit final : public ClassUnit {
    public:
        explicit CSClassUnit(std::string name) : ClassUnit(std::move(name)) {}

        std::string compile(unsigned int level = 0) const override {
            std::stringstream result;
            result << generateShift(level) << "class " << className << " {" << std::endl;

            for (size_t i = 0; i < ACCESS_MODIFIERS.size(); i++) {
                if (!fields[i].empty()) {
                    for (auto &it : fields[i]) {
                        result << std::endl << generateShift(level+1)
                               << ACCESS_MODIFIERS[i] << " " << it->compile(level+1);
                    }
                }
            }
            result << generateShift(level) << "}" << std::endl;
            return result.str();
        }

    };


    class CSMethodUnit final : public MethodUnit {
    public:
        CSMethodUnit(std::string name, std::string returnType, Flag flags)
                : MethodUnit(std::move(name), std::move(returnType), flags) {}

        std::string compile(unsigned int level) const override {
            std::stringstream result;
            if (flags & STATIC) {
                result << "static ";
            } else if (!(flags & VIRTUAL)) {
                result << "final ";
            }

            // TODO: check it
            result << returnType << " " << methodName << "() {" << std::endl;

            for (auto &it : body) {
                result << it->compile(level + 1);
            }

            result << generateShift(level) << '}' << std::endl;

            return result.str();
        }

    };


    class CSPrintOperatorUnit final : public PrintOperatorUnit {
    public:
        explicit CSPrintOperatorUnit(std::string text) : PrintOperatorUnit(std::move(text)) {}

        std::string compile(unsigned int level) const override {
            return generateShift(level) + "Console.print(\"" + text + "\");\n";
        }

    };


    class CSReturnOperatorUnit final : public ReturnOperatorUnit {
    public:
        explicit CSReturnOperatorUnit(const std::string &returnValue = "")
                : ReturnOperatorUnit(returnValue) {}

        std::string compile(unsigned int level) const override {
            auto result = generateShift(level) + "return";
            if (!value.empty())
                result += " " + value;
            return result + ";\n";
        }
    };


    struct CSModel;
    template <> struct LangModel<CSModel> {
        using ClassUnitType = CSClassUnit;
        using MethodUnitType = CSMethodUnit;
        using PrintOperatorUnitType = CSPrintOperatorUnit;
        using ReturnOperatorUnitType = JavaReturnOperatorUnit;
    };

    using CSLangFabric = LangFabric<CSModel>;
}

#endif //SPPLABC_CS_MODEL_HPP
