//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_CPP_MODEL_HPP
#define SPPLABC_CPP_MODEL_HPP

#include "unit.hpp"
#include "class_unit.hpp"
#include "method_unit.hpp"
#include "print_op_unit.hpp"
#include "return_op_unit.hpp"
#include "model.hpp"

namespace spp {

    class CPPClassUnit final : public ClassUnit {
    public:
        explicit CPPClassUnit(std::string name) : ClassUnit(std::move(name)) {}

        std::string compile(unsigned int level = 0) const override {
            std::stringstream result;
            result << generateShift(level) << "class " << className << " {" << std::endl;

            for (size_t i = 0; i < ACCESS_MODIFIERS.size(); i++) {
                if (!fields[i].empty()) {
                    result << ACCESS_MODIFIERS[i] << ":";
                    for (auto &it : fields[i]) {
                        result << std::endl << it->compile(level+1);
                    }
                    result << std::endl;
                }
            }
            result << generateShift(level) << "};" << std::endl;
            return result.str();
        }

    };


    class CPPMethodUnit final : public MethodUnit {
    public:
        CPPMethodUnit(std::string name, std::string returnType, Flag flags)
            : MethodUnit(std::move(name), std::move(returnType), flags) {}

        std::string compile(unsigned int level) const override {
            std::stringstream result;
            result << generateShift(level);
            if (flags & STATIC) {
                result << "static ";
                if (flags & CONST) {
                    throw InvalidModelException("static method cannot be const method");
                }
            } else if (flags & VIRTUAL) {
                result << "virtual ";
            }

            result << returnType << " " << methodName << "()";

            if (flags & CONST) {
                result << " const";
            }

            result << " {" << std::endl;

            for (auto &it : body) {
                result << it->compile(level + 1);
            }

            result << generateShift(level) << '}' << std::endl;

            return result.str();
        }

    };


    class CPPPrintOperatorUnit final : public PrintOperatorUnit {
    public:
        explicit CPPPrintOperatorUnit(std::string text) : PrintOperatorUnit(std::move(text)) {}

        std::string compile(unsigned int level) const override {
            return generateShift(level) + "printf(\"" + text + "\");\n";
        }

    };


    class CPPReturnOperatorUnit final : public ReturnOperatorUnit {
    public:
        explicit CPPReturnOperatorUnit(const std::string &returnValue = "")
            : ReturnOperatorUnit(returnValue) {}

        std::string compile(unsigned int level) const override {
            auto result = generateShift(level) + "return";
            if (!value.empty())
                result += " " + value;
            return result + ";\n";
        }
    };


    struct CPPModel;
    template <> struct LangModel<CPPModel> {
        using ClassUnitType = CPPClassUnit;
        using MethodUnitType = CPPMethodUnit;
        using PrintOperatorUnitType = CPPPrintOperatorUnit;
        using ReturnOperatorUnitType = CPPReturnOperatorUnit;
    };

    using CPPLangFabric = LangFabric<CPPModel>;
}

#endif //SPPLABC_CPP_MODEL_HPP
