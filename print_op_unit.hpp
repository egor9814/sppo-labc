//
// Created by egor9814 on 3/22/19.
//

#ifndef SPPLABC_PRINT_OP_UNIT_HPP
#define SPPLABC_PRINT_OP_UNIT_HPP

#include "unit.hpp"

namespace spp {

    class PrintOperatorUnit : public Unit {
    protected:
        std::string text;

    public:
        explicit PrintOperatorUnit(std::string text) : text(std::move(text)) {}

    };

}

#endif //SPPLABC_PRINT_OP_UNIT_HPP
